let _start_date = new Date(0);
let _stop_date = new Date(0);
let _date = new Date(0);
let _remaining_time = new Date(0);
let _timer = 0;
let _interval_id = null;
let _interval_id_ms = null;
let _interval_id_temp = null;
let _interval_id_temp_ms = null;
let _interval_background_change = null;
let _timer_list = [];
let _is_red_background = false;
let _min = 0;
let _sec = 0;
let _ms = 0;

const CronometroType = 0;
const TemporizadorType = 1;

let _timer_type = CronometroType;
updateTimer();

function pad(num, size) {
    var s = "00" + num;
    return s.substr(s.length-size);
}

let _span_min = document.createElement("span");
let _span_sec = document.createElement("span");
let _span_ms = document.createElement("span");

_span_min.addEventListener('click', () =>
{
    if(_interval_id_temp_ms != null || _interval_id_ms != null)
       return;

    _min = prompt("Insert minutes");

    if(_min < 0)
        _min = 0;
    
     if(_min > 59)
         _min = 59;
    updateTimer();
});

_span_sec.addEventListener('click', () =>
{
    if(_interval_id_temp_ms != null || _interval_id_ms != null)
        return;

    _sec = prompt("Insert seconds");

    if(_sec < 0)
     _sec = 0;
    
     if(_sec > 59)
        _sec = 59;
    updateTimer();
});

_span_ms.addEventListener('click', () =>
{
    if(_interval_id_temp_ms != null || _interval_id_ms != null)
        return;

    _ms = prompt("Insert milliseconds");

    if(_ms < 0)
        _ms = 0;
    
     if(_ms > 999)
        _ms = 999;

    updateTimer();
});

function updateTimer()
{
    let date = null;
    document.getElementById("timer").innerHTML = _timer_type == CronometroType ? _timer : "";

    document.getElementById("date").innerHTML = "";
    if(_timer_type == CronometroType)
        return;

    _span_min.innerHTML = pad(_min, 2) + ':';
    _span_sec.innerHTML = pad(_sec, 2) + ':';
    _span_ms.innerHTML = pad(_ms, 3);

    document.getElementById("date").appendChild(_span_min);
    document.getElementById("date").appendChild(_span_sec);
    document.getElementById("date").appendChild(_span_ms);
}

function incrementTimer(is_timer)
{
    _timer++;

    if(!is_timer && _timer_type == TemporizadorType)
    {
        // can't increment while running
        if(_interval_id_temp_ms != null || _interval_id_ms != null)
            return;

        ++_sec;
        console.log("si" + _sec);
        if(_sec >= 60)
        {
            _min += 1;
            _sec = 0;

            if(_min >= 60)
                _min = 0;
        }
    }

    updateTimer();

    if(_timer >= 0 && _timer_type == CronometroType || 
        (_timer_type == TemporizadorType && (_min > 0 || _sec > 0 || _ms > 0)))
    {
        if(_interval_id_temp == null)
            document.getElementById("timer_button").disabled = false;

        if(_interval_id == null)
            document.getElementById("temp_button").disabled = false;
    }
}

function decrementTimer(is_timer)
{
    _timer--;

    if(_timer_type == TemporizadorType)
    {
        if(!is_timer)
        {
            // can't decrement while running
            if(_interval_id_temp_ms != null || _interval_id_ms != null)
                return;
            
            if(_min == 0 && _sec == 0 && _ms > 0)
            {
                _ms = 0;
            }
            else
            {
                --_sec;
                if(_sec < 0)
                {
                    if(_min > 0)
                        --_min;
                    else if (_min == 0)
                        _sec = 0;
                    else
                        _sec = 59;
                }
            }
        }

        updateTimer();
        return;
    }

    updateTimer();

    if(_timer <= 0)
    {
        document.getElementById("temp_button").disabled = true;
       
            document.getElementById("timer_button").disabled = true;
            document.getElementById("timer_button").innerHTML = "Iniciar Cronometro";
            if(_interval_id_temp != null)
                clearTemp();
        
    }
}

function resetTimer()
{
    _min = _sec = _ms = 0;
    _timer = 0;
    document.getElementById("timer_button").disabled = false;
    
    document.getElementById("timer_button").innerHTML = "Iniciar Cronometro";
    document.getElementById("temp_button").innerHTML = "Iniciar Temporizador";
    
    document.getElementById("temp_button").disabled = true;

    document.getElementById("timer_inc").disabled = false;
    document.getElementById("timer_dec").disabled = false;

    document.querySelector("body").style.backgroundColor = "#FFFFFF";
    _is_red_background = false;

    clearInterval(_interval_id);
    _interval_id = null;
    clearInterval(_interval_id_ms);
    _interval_id_ms = null;
    clearInterval(_interval_id_temp);
    _interval_id_temp = null;
    clearInterval(_interval_id_temp_ms);
    _interval_id_temp_ms = null;
    clearInterval(_interval_background_change);
    _interval_background_change = null;
    updateTimer();
}

function clearTimer(from_check)
{
    clearInterval(_interval_id);
    _interval_id = null;
    clearInterval(_interval_id_ms);
    _interval_id_ms = null;
    if(!from_check)
    {
        document.getElementById("timer_button").innerHTML = "Iniciar Cronometro";
        document.getElementById("turn_button").style.visibility = "hidden";
        if((_timer > 0 && _timer_type == CronometroType) ||
            (_timer_type == TemporizadorType && (_min > 0 || _sec > 0 || _ms > 0)))
            document.getElementById("temp_button").disabled = false;
    }
}

function triggerTimer()
{
    if(_interval_id != null)
    {
        _stop_date = Date.now() - _start_date;
        clearTimer(false);
        
        _remaining_time = new Date(Date.now() - _start_date.getTime());
        return;
    }
    else if((_timer >= 0 && _timer_type == CronometroType) || 
        (_timer_type == TemporizadorType))
    {
        document.getElementById("timer_button").innerHTML = "Detener Cronometro";
        document.getElementById("turn_button").style.visibility = "visible";
    }

    function increment(is_interval)
    {
        if(_timer < 0)
        {
            clearTimer(true);
            return;
        }

        document.getElementById("temp_button").disabled = true;
        if(is_interval)
            incrementTimer(true);
    }

    increment(false);
    _interval_id = setInterval(() => 
    {
        increment(true);
    }, 1000);

    _start_date = new Date(Date.now() + _remaining_time.getTime());
    _interval_id_ms = setInterval(() => 
    {   
        _ms += 10;
        if(_ms > 999)
        {
            _ms = 0;
            ++_sec;
            if(_sec >= 60)
            {
                _min += 1;
                _sec = 0;
    
                if(_min >= 60)
                    _min = 0;
            }
        }

        updateTimer();
    }, 10);
}

function updateTimerList()
{
    let ul = document.getElementById("timer_list");
    ul.innerHTML = "";

    for(let i = 0; i < _timer_list.length; i++)
    {
        let value = _timer_list[i].value;
        if(_timer_type == TemporizadorType)
        {
            value = pad(_timer_list[i].min, 2) + ":" + pad(_timer_list[i].sec, 2) + ":" + pad(_timer_list[i].ms, 2);
        }

        let diff = _timer_list[i].diff;
        if(_timer_type == TemporizadorType)
        {
            let diff_date = _timer_list[i].diff_date;
            diff = pad(diff_date.getMinutes(), 2) + ":" + pad(diff_date.getSeconds(), 2) + ":" +pad(diff_date.getMilliseconds(), 2);
        }

        _timer_list[i].elem.innerHTML = _timer_list[i].idx + ": " + value  +
        " - diff: " + diff + " - name: " + _timer_list[i].name;

        ul.appendChild(_timer_list[i].elem);
    }
}

function addNameToTimerItem(ev, idx)
{
    _timer_list[idx].name = prompt("Ingrese un nombre");

    updateTimerList();
}

function turnTimer()
{
    let diff = 0;
    if(_timer_list.length > 1)
    {
        diff = _timer - _timer_list[_timer_list.length - 2].value;
    }

    function handleEvent(passedInElement) {
        return function(e) {
            addNameToTimerItem(e, passedInElement); 
        };
    }

    let elem = document.createElement('li');
    elem.addEventListener('click', handleEvent(_timer_list.length));

    _timer_list.push({
        value: _timer,
        diff: diff,
        idx: _timer_list.length,
        elem: elem,
        name: '',
        min: _min,
        sec: _sec,
        ms: _ms,
        date: new Date(Date.now()),
        diff_date: _timer_list.length > 1 ? new Date(Date.now() - _timer_list[_timer_list.length - 2].date.getTime()) : new Date(0)
    });

    updateTimerList();
}

function deleteTimerList()
{
    _timer_list = [];
    updateTimerList();
}

function clearTemp()
{
    _remaining_time = new Date(0);
    if(_timer_type == CronometroType)
        document.getElementById("temp_button").innerHTML = _timer == 0 ? "Temporizador OK" : "Iniciar Temporizador";
    else
        document.getElementById("temp_button").innerHTML = _min == 0 && _sec == 0 && _ms == 0 ? "Temporizador OK" : "Iniciar Temporizador";

    clearInterval(_interval_id_temp);
    _interval_id_temp = null;
    clearInterval(_interval_id_temp_ms);
    _interval_id_temp_ms = null;
    document.getElementById("timer_button").disabled = false;
    document.getElementById("turn_button").style.visibility = "hidden";

    if((_timer <= 0 && _timer_type == CronometroType) || 
        (_timer_type == TemporizadorType && (_min == 0 && _sec == 0 && _ms == 0)))
    {
        document.getElementById("temp_button").disabled = false;
        document.getElementById("timer_button").disabled = true;
        document.getElementById("timer_inc").disabled = true;
        document.getElementById("timer_dec").disabled = true;

        _interval_background_change = setInterval(() =>
            {
                let el = document.querySelector("body");
                if(_is_red_background)
                {
                    el.style.backgroundColor = "#FFFFFF";
                    _is_red_background = false;
                }
                else 
                {
                    el.style.backgroundColor = "#D50000";
                    _is_red_background = true;
                }
            }, 2000);
    
        _min = _sec = _ms = 0;
    }
}

function triggerTemp()
{
    if(_interval_background_change != null)
    {
        resetTimer();

        
        /*
        document.getElementById("timer_button").disabled = false;
        document.getElementById("timer_inc").disabled = false;
        document.getElementById("timer_dec").disabled = false;
        document.getElementById("temp_button").innerHTML = "Iniciar Temporizador";
    
        clearInterval(_interval_background_change);
        _interval_background_change = null;
        */
        return;
    }

    if(_interval_id_temp != null)
    {
        document.getElementById("turn_button").style.visibility = "hidden";
        clearTemp();
        //_remaining_time = new Date(Date.now() - _start_date.getTime());
        return;
    }
    else if((_timer_type == CronometroType && _timer > 0) ||
        (_timer_type == TemporizadorType && (_sec > 0 || _min > 0 || _ms > 0)))
    {
        document.getElementById("temp_button").innerHTML = "Detener Temporizador";
        document.getElementById("turn_button").style.visibility = "visible";
    }

    function decrement(is_interval)
    {
        if(_timer <= 0)
        {
            clearTemp();
            return;
        }

        document.getElementById("timer_button").disabled = true;
        if(is_interval)
            decrementTimer(true);
    }

    decrement(false);
    _interval_id_temp = setInterval(() => 
    {
        decrement(true)
    }, 1000);

    _start_date = Date.now();
    _interval_id_temp_ms = setInterval(() => 
    {
        _ms -= 10;
        if(_ms < 0)
        {
            _ms = 999;
            --_sec;
            if(_sec < 0)
            {
                _sec = 0;
                --_min;
                if(_min < 0)
                {
                    _min = 0;
                    _ms = 0;
                    clearTemp();
                }
            }
        }
       updateTimer();
    }, 10);
}

function setTimerType(type)
{
    _timer_type = type;
    updateTimer();
    resetTimer();
    _timer_list = [];
    updateTimerList();
}

